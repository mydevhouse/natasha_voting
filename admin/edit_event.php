<?php include 'header.php';?>

<?php 

				
if($_SERVER['REQUEST_METHOD']=="POST"){

	$upload_dir = '../images/';
	$id = $_POST['id'];	
		$ename = $_POST['ename'];
		$ename2 = $_POST['eventxx'];
		$edetails  = $_POST['edetails'];
		$edate = $_POST['edate'];
		$edate = htmlentities($edate);
		$edate = date('Y-m-d', strtotime($edate));
		$elocation = $_POST['elocation'];
		$ecat  = $_POST['ecat'];
		

		$fileName = $_FILES['eimg']['name'];
		$tmpName = $_FILES['eimg']['tmp_name'];
		$fileSize = $_FILES['eimg']['size'];
		$fileType = $_FILES['eimg']['type'];
		$filePath =  $upload_dir . $fileName;
		$result = move_uploaded_file($tmpName, $filePath);
		if(isset($_FILES["eimg"])){

			if($result){

				$str= "UPDATE `tbl_event` SET `name`=:a,`details`=:b,`date`=:c,`location`=:d,`image`=:e,`cat`=:f WHERE ID=$id";
				$cm=$conn->prepare($str);
				$cm->bindvalue(':a', $ename);
				$cm->bindvalue(':b', $edetails);
				$cm->bindvalue(':c', $edate);
				$cm->bindvalue(':d', $elocation);
				$cm->bindvalue(':e', $fileName);
				$cm->bindvalue(':f', $ecat);
			#$cm->execute();
				if ($cm->execute()){
				$str= "UPDATE `tbl_participants` SET `events`=:a WHERE `events`='$ename2'";
				$cm=$conn->prepare($str);
				$cm->bindvalue(':a', $ename);
				$cm->execute();
					header("location: list_event.php");
				}else{
					die();
				}

			}else{echo"result not executed <br>".$tmpName;}
			

		}else{
			echo"nofile";
			die();}

		
	

}

				if(isset($_GET['ID'])){
					$id = $_GET['ID'];
					$st="SELECT * FROM `tbl_event` where ID=$id";
					$cm=$conn->prepare($st);
					$cm->execute();
					while($row = $cm->fetch(PDO::FETCH_ASSOC)){
						$name = $row['name'];
						$details = $row['details'];
						$date = $row['date'];
						$location = $row['location'];
					}



				}else{die();}



				
				?>


<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h2">Edit Event</h1>
          </div>
			<form class="" action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" method="post">
			<input type="hidden" value="<?php echo $id ;?>" name="id">
			<input type="hidden" name="eventxx" id=""value="<?php echo $name ;?>">
			  <div class="form-group">
				<label for="event_name">Event Name</label>
				<input type="text" class="form-control" value="<?php echo $name;?>"id="participant_name" name="ename">
			  </div>
		<div class="form-group">
    <label for="exampleFormControlFile1">Featured Image</label>
    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="eimg">
  </div>
			  <div class="form-group">
				  <label for="event details">Event Details</label>
				  <textarea class="form-control" rows="5" id="details" name="edetails"><?php echo $details;?></textarea>
				</div>
				<div class="form-group">
				  <label for="category">Category </label>
				  <select name="ecat" class="form-control" id="category">
					<?php 
$st = "SELECT * FROM `tbl_category`";
$cm=$conn->prepare($st);
$cm->execute();
while($row = $cm->fetch(PDO::FETCH_ASSOC)){
	?><option value="<?php echo $row['cat'];?>"><?php echo $row['cat'];?></option><?php
}
?>
				  </select>
				 </div>
				 <div class="form-group">
					<label for="scheduled_date">Scheduled Date</label>
					<input name="edate" type="date" class="form-control" id="scheduled_date">
				  </div>
					<div class="form-group">
					<label for="location">Location</label>
					<input type="text" name="elocation" class="form-control" id="location" value="<?php echo $location;?>">
				  </div>
			  <button type="submit" class="btn btn-info">ADD</button>
			</form>
			
          </div>
</main>
<?php include 'footer.php';?>