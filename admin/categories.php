<?php include 'header.php';?>

<?php 
if ($_SERVER['REQUEST_METHOD']=="POST"){

	$c = strip_tags($_POST['cat']);
	

	if (isset($c)){

	try {
		$str= "INSERT INTO `tbl_category`(`cat`) VALUES (:c)";
		$cm=$conn->prepare($str);
		$cm->bindvalue(':c', $c);
		if ($cm->execute()){
			header("Refresh:0");
		}else{
			die();
		}

	} catch (Exception $e) {
		echo 'error  '.$e ->getmessage();
	}
	
	}

}
?>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h2">CATEGORIES</h1>
          </div>
			<form class="form-inline" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
			  <div class="form-group">
				<input type="text" class="form-control" id="category" name="cat">
			  </div>
			  <button type="submit" class="btn btn-info">ADD</button>
			</form>
			<br>
			  <div class="table-responsive">
			  <table class="table table-striped table-sm">
					<tr>
						<th>ID</th>
						<th>Category</th>
						<th>Action</th>
					</tr>
					
<?php 
$st = "SELECT * FROM `tbl_category`";
$cm=$conn->prepare($st);
$cm->execute();
while($row = $cm->fetch(PDO::FETCH_ASSOC)){
	?>
<tr>
						<td><?php echo $row['ID'];?></td>
						<td><?php echo $row['cat'];?></td>
						<td><a class="btn btn-danger" href="?ID=<?php echo $row['ID'];?>">DELETE</a></td>
					</tr>
	<?php
}
?>

				</table>
          </div>
</main>

<?php 
if(isset($_GET['ID'])){
	$id=$_GET['ID'];
	$str= "DELETE FROM `tbl_category` WHERE ID=$id";
	$cm=$conn->prepare($str);
	$cm->execute();
	header("location:categories.php");
}
?>
<?php include 'footer.php';?>