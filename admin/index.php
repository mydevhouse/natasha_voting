<?php include 'header.php';?>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h2">Dashboard</h1>
          </div>
			
          <h2>Overview</h2>
		  <div class="form-group">
				<input type="text" class="form-control" id="search" placeholder="SEARCH">
			  </div>
          <div class="table-responsive">
		  <table class="table table-striped table-sm">
				<tr>
					<th>Event Name</th>
					<th>Participants</th>
					<th>Votes</th>
					<th>Total Votes</th>
				</tr>

<?php 
				$st = "SELECT * FROM `tbl_event`";
				$cm=$conn->prepare($st);
				$cm->execute();
				while($row = $cm->fetch(PDO::FETCH_ASSOC)){
?>
<tr>
	<td><?php echo $row['name'];?></td>
<!-- participant nest	 -->
<td>
	<?php 
				$n = $row['name'];
				$stx = "SELECT * FROM `tbl_participants` where events='$n'";
				$cmx=$conn->prepare($stx);
				$cmx->execute();
				while($rowx = $cmx->fetch(PDO::FETCH_ASSOC)){
				if(isset($rowx['name'])){
					$parray[] = $rowx['name'];
				}
				
	?>
	<div class="col-6 <?php echo $rowx['name'];?>"><?php echo $rowx['name'];?></div>

	<?php  } ?>
</td>


<td>

<?php 
// echo  sizeof($parray);
// print_r($parray);
$ename = $row['name'];
// echo $ename;
$y = sizeof($parray);
$y = $y -1;
if(isset($y)){
	
	for ($x = 0; $x <= $y; $x++) {
		$pname = $parray[$x];
		// echo $pname."<br>";
		// echo $ename."<br>";
		$string= "SELECT count(*) as v FROM `tbl_vote` WHERE event='$ename' and pname='$pname'";
		$command = $conn->prepare($string);
		$command->execute();
		while($vote = $command->fetch(PDO::FETCH_ASSOC)){
			?>
			<div class="col-6 <?php echo $vote['v'];?>"><?php echo $vote['v'];?></div>
			<?php
		}
		
	} 
}else{
	echo "NO DATA";
}
?>
</td>

<td>
<!-- total -->
<?php 

$string= "SELECT count(*) as v FROM `tbl_vote` WHERE event='$ename' ";
		$command = $conn->prepare($string);
		$command->execute();
		while($vote = $command->fetch(PDO::FETCH_ASSOC)){
			?>
			<div class="col-6 <?php echo $vote['v'];?>"><?php echo $vote['v'];?></div>
			<?php
		}

?>
</td>

</tr>



<?php
//clearing array 
unset($parray);
} ?>
<!--
				<tr>
					<td>DOTA</td>
					<td>
						<div class="col-6">TNC Featured image</div>
						<div class="col-6">OG Featured image</div>
						<div class="col-6">VG Featured image</div>
						<div class="col-6">EG Featured image</div>
						<div class="col-6">VP Featured image</div>
					</td>
					<td>
						<div class="col-6">200</div>
						<div class="col-6">102</div>
						<div class="col-6">103</div>
						<div class="col-6">104</div>
						<div class="col-6">105</div>
					
					</td>
					<td><strong>1000</strong></td>
				</tr>
-->
			</table>
          </div>
        </main>

<?php include 'footer.php';?>
