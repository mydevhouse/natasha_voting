<?php include 'header.php';?>

<body class="<?php echo basename($_SERVER['PHP_SELF']);?>">
	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Event</a>
					</li>
				  
				</ul>
			</div>
		</nav>
	</div>


<div class="container">
	<div class="row">
		<div class="col-12">
			<h2>EVENTS</h2>
			<div class="form-group">
				<input type="text" class="form-control" id="search" placeholder="SEARCH">
			  </div>
          <div class="table-responsive">
		  <table class="table table-striped table-sm">
				<tr>
					<th><center>EVENT NAME</center></th>
					<th>PARTICIPANTS</th>
					<th>VOTES</th>
					<th>TOTAL</th>
					<th></th>
				</tr>
				<?php 
				$st = "SELECT * FROM `tbl_event`";
				$cm=$conn->prepare($st);
				$cm->execute();
				while($row = $cm->fetch(PDO::FETCH_ASSOC)){
?>
<tr>
	<td><center><h2><?php echo $row['name'];?></h2><br>
	<img src="images/<?php echo $row['image']; ?>" alt="" class="img-responsive" height="150"><br><br><br>
	Scheduled Date: <strong><?php echo $row['date'];?></strong><center></td>
<!-- participant nest	 -->
<td>
	<?php 
				$n = $row['name'];
				$stx = "SELECT * FROM `tbl_participants` where events='$n'";
				$cmx=$conn->prepare($stx);
				$cmx->execute();
				while($rowx = $cmx->fetch(PDO::FETCH_ASSOC)){
				if(isset($rowx['name'])){
					$parray[] = $rowx['name'];
				}
				
	?>
	<img src="images/<?php echo $rowx['image']; ?>" alt="" class="img-responsive" height="150">
	<div class="col-md-6 <?php echo $rowx['name'];?>"><center><strong><?php echo $rowx['name'];?></strong></center></div>
	<br>
	<p><?php echo $rowx['details'];?></p>
	<br>
	<br>
	<?php  } ?>
</td>



<td>

<?php 
// echo  sizeof($parray);
// print_r($parray);
$ename = $row['name'];
// echo $ename;
$y = sizeof($parray);
$y = $y -1;
if(isset($y)){
	
	for ($x = 0; $x <= $y; $x++) {
		$pname = $parray[$x];
		// echo $pname."<br>";
		// echo $ename."<br>";
		$string= "SELECT count(*) as v FROM `tbl_vote` WHERE event='$ename' and pname='$pname'";
		$command = $conn->prepare($string);
		$command->execute();
		while($vote = $command->fetch(PDO::FETCH_ASSOC)){
			?>
			<div class="col-6 <?php echo $vote['v'];?>"><?php echo $vote['v'];?></div>
			<?php
		}
		
	} 
}else{
	echo "NO DATA";
}
?>
</td>

<td>
<!-- total -->
<?php 

$string= "SELECT count(*) as v FROM `tbl_vote` WHERE event='$ename' ";
		$command = $conn->prepare($string);
		$command->execute();
		while($vote = $command->fetch(PDO::FETCH_ASSOC)){
			?>
			<div class="col-6 <?php echo $vote['v'];?>"><?php echo $vote['v'];?></div>
			<?php
		}

?>
</td>

</td>

<td>
<a href="vote.php?event=<?php echo $row['name']; ?>" class="btn btn-danger btn-lg">VOTE</a>
</td>

</tr>



<?php
//clearing array 
unset($parray);
} ?>
			
			</table>
		</div>
		</div>
	 </div>
 </div>
 <?php include 'footer.php';?>